﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ConsolePlayThreads
{
    class Program
    {
        private static object lockControl = new object();
        static void Main(string[] args)
        {
            Main1();
        }

        static void Main1()
        {
            var cli = Container.Instance.ClientUser;

            var t1 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("t1");
                Thread.Sleep(3000);
                Console.WriteLine("t1 Executou");
                if (cli.CheckReg()) 
                {
                    Console.WriteLine($"t1 {cli.IsRegistered.ToString()}");
                    Console.WriteLine("t1 Executou e Entrou");
                }
            });
            var t2 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("t2");
                Thread.Sleep(2995);
                Console.WriteLine("t2 Executou");
                if (cli.CheckReg()) 
                {
                    Console.WriteLine($"t2 {cli.IsRegistered.ToString()}");
                    Console.WriteLine("t2 Executou e Entrou");
                }
            });
            var t3 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("t3");
                Thread.Sleep(3005);
                Console.WriteLine("t3 Executou");
                if (cli.CheckReg()) 
                {
                    Console.WriteLine($"t3 {cli.IsRegistered.ToString()}");
                    Console.WriteLine("t3 Executou e Entrou");
                }
            });

            try
            {
                Task.WaitAll(t1, t2, t3);
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }

    class Container
    {
        private static readonly Lazy<Container> lazy = new Lazy<Container>(() => new Container());

        private ConcurrentDictionary<int, ClientUser> dictionary = new ConcurrentDictionary<int, ClientUser>();

        public static Container Instance { get { return lazy.Value; } }

        private Container()
        {
            this.ClientUser = new ClientUser();
        }

        public ClientUser ClientUser
        {
            get
            {
                ClientUser user = null;
                dictionary.TryGetValue(0, out user);
                return user;
            }
            set
            {
                dictionary.TryAdd(0, value);
            }
        }
    }

    public class ClientUser
    {
        private int isReg = 0;

        public bool IsRegistered
        {
            get
            {
                return isReg == 1;
            }
            set
            {
                isReg = value ? 1 : 0;
            }
        }

        // Des/Comentar para comparar
        //public bool CheckReg()
        //{
        //    bool check = false;
        //    if (isReg == 0)
        //    {
        //        isReg = 1;
        //        check = true;
        //    }

        //    return check;
        //}

        // Des/Comentar para comparar
        //public bool CheckReg()
        //{
        //    return Interlocked.CompareExchange(ref isReg, 1, 0) == 0;
        //}

        //Des/Comentar para comparar
        public bool CheckReg()
        {
            var ret = Volatile.Read(ref isReg) == 0 ? isReg++ : isReg;
            return ret == 0;
        }
    }
}
